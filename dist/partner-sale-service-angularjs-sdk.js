(function () {
    angular.module(
        "partnerSaleServiceModule",
        [
        ]);
})();

/**
 * A mailing address
 * @typedef {Object} Address
 * @property {string} line1
 * @property {string} [line2]
 * @property {string} city
 * @property {string} region
 * @property {string} postalCode
 * @property {string} countryCode - iso 3166-1-alpha-2 country code
 */

/**
 * Information about a contact for a facility
 * @typedef {Object} FacilityContactInformation
 * @property {string} firstName
 * @property {string} lastName
 * @property {string} phoneNumber
 * @property {string} emailAddress
 */

/**
 * Information about a facility
 * @typedef {Object} FacilityInformation
 * @property {string} name
 * @property {string} salesforceId
 * @property {Address} address
 * @property {string} phoneNumber
 * @property {string} customerSegmentLevel1
 * @property {string} customerSegmentLevel2
 * @property {string} customerSegmentLevel3
 * @property {string} brand
 * @property {string} subBrand,
 * @property {FacilityContactInformation} contact
 */

/**
 * An invoice from a partners sale
 * @typedef {Object} PartnerSaleInvoice
 * @property {string} url
 * @property {string} number
 */

/**
 * A partner sale
 * @typedef {Object} PartnerSale
 * @property {number} timestamp
 * @property {PartnerSaleInvoice} invoice
 * @property {string} repId
 * @property {PartnerSaleLineItem[]} lineItems
 * @property {string} registrationId
 * @property {number} registrationTimestamp
 */

/**
 * A partner commercial sale
 * @typedef {Object} PartnerCommercialSale
 * @augments PartnerSale
 * @property {string} customerSource
 * @property {string} managementCompany
 * @property {string} buyingGroup
 * @property {FacilityInformation} facility
 */

/**
 * Information about a consumer
 * @typedef {Object} ConsumerInformation
 * @property {string} firstName
 * @property {string} lastName
 * @property {Address} address
 * @property {string} phoneNumber
 * @property {string} emailAddress
 */

/**
 * A partner consumer sale
 * @typedef {Object} PartnerConsumerSale
 * @augments PartnerSale
 * @property {ConsumerInformation} consumer
 */

/**
 * @typedef {Object} PartnerSaleLineItem
 * @property {number} [price]
 */

/**
 * @typedef {Object} PartnerSaleCompositeLineItem
 * @augments PartnerSaleLineItem
 * @property {PartnerSaleSimpleLineItem[]} [components]
 */

/**
 * @typedef {Object} PartnerSaleSimpleLineItem
 * @augments PartnerSaleLineItem
 * @property {string} productGroup
 * @property {string} productLine
 * @property {string} serialNumber
 * @property {string} description
 */

/**
 * A PartnerCommercialSale summary
 * @typedef {Object} PartnerCommercialSaleSummary
 * @property {string} facilityName
 * @property {string} facilityAddress
 * @property {string} registrationTimestamp
 * @property {string} registrationId
 */
(function () {
    angular
        .module("partnerSaleServiceModule")
        .provider(
        'partnerSaleServiceConfig',
        partnerSaleServiceConfigProvider
    );

    function partnerSaleServiceConfigProvider() {

        var objectUnderConstruction = {
            setBaseUrl: setBaseUrl,
            $get: $get
        };

        return objectUnderConstruction;

        function setBaseUrl(baseUrl) {
            objectUnderConstruction.baseUrl = baseUrl;
            return objectUnderConstruction;
        }

        function $get() {
            return {
                baseUrl: objectUnderConstruction.baseUrl
            }
        }
    }
})();
(function () {
    angular
        .module('partnerSaleServiceModule')
        .factory(
        'partnerSaleServiceClient',
        [
            'partnerSaleServiceConfig',
            '$http',
            '$q',
            partnerSaleServiceClient
        ]);

    function partnerSaleServiceClient(partnerSaleServiceConfig,
                                      $http,
                                      $q) {

        var lastRegisteredPartnerCommercialSale,
            lastRegisteredPartnerConsumerSale;

        return {
            constructPartnerCommercialSale: constructPartnerCommercialSale,
            constructPartnerConsumerSale: constructPartnerConsumerSale,
            constructPartnerSaleCompositeLineItem: constructPartnerSaleCompositeLineItem,
            constructPartnerSaleSimpleLineItem: constructPartnerSaleSimpleLineItem,
            getPartnerSale: getPartnerSale,
            getPartnerCommercialSale: getPartnerCommercialSale,
            getPartnerConsumerSale: getPartnerConsumerSale,
            listCurrentPartnerCommercialSalesRegisteredSinceTimestamp: listCurrentPartnerCommercialSalesRegisteredSinceTimestamp,
            registerPartnerCommercialSale: registerPartnerCommercialSale,
            registerPartnerConsumerSale: registerPartnerConsumerSale,
            updatePartnerSaleInvoice: updatePartnerSaleInvoice
        };

        /**
         * Constructs a PartnerCommercialSale
         * @returns {PartnerCommercialSale}
         */
        function constructPartnerCommercialSale() {
            return {
                timestamp: null,
                invoice: null,
                customerSource: null,
                managementCompany: null,
                buyingGroup: null,
                facility: {
                    name: null,
                    salesforceId: null,
                    address: {
                        line1: null,
                        line2: null,
                        city: null,
                        region: null,
                        postalCode: null,
                        countryCode: null
                    },
                    phoneNumber: null,
                    customerSegmentLevel1: null,
                    customerSegmentLevel2: null,
                    customerSegmentLevel3: null,
                    brand: null,
                    subBrand: null,
                    contact: {
                        firstName: null,
                        lastName: null,
                        phoneNumber: null,
                        emailAddress: null
                    }
                },
                repId: null,
                lineItems: [],
                registrationId: null,
                registrationTimestamp: null
            };
        }

        /**
         * Constructs a PartnerConsumerSale
         * @returns {PartnerConsumerSale}
         */
        function constructPartnerConsumerSale() {
            return {
                timestamp: null,
                invoice: null,
                consumer: {
                    firstName: null,
                    lastName: null,
                    address: {
                        line1: null,
                        line2: null,
                        city: null,
                        region: null,
                        postalCode: null,
                        countryCode: null
                    },
                    phoneNumber: null,
                    emailAddress: null
                },
                repId: null,
                lineItems: [],
                registrationId: null,
                registrationTimestamp: null
            };
        }

        /**
         * Constructs a PartnerSaleCompositeLineItem
         * @returns {PartnerSaleCompositeLineItem}
         */
        function constructPartnerSaleCompositeLineItem() {
            return {
                price: null,
                components: []
            };
        }

        /**
         * Constructs a PartnerSaleSimpleLineItem
         * @returns {PartnerSaleSimpleLineItem}
         */
        function constructPartnerSaleSimpleLineItem() {
            return {
                productGroup: null,
                productLine: null,
                serialNumber: null,
                description: null,
                price: null
            };
        }

        /**
         * Gets a partner sale  by its registrationId
         * @param registrationId
         * @returns {PartnerSale}
         */
        function getPartnerSale(registrationId) {
            // use local data if we have it
            if (lastRegisteredPartnerCommercialSale
                && lastRegisteredPartnerCommercialSale.registrationId == registrationId) {
                var deferred = $q.defer();
                deferred.resolve(lastRegisteredPartnerCommercialSale);
                return deferred.promise;
            }
            else {
                // use dummy data for now
                var deferred = $q.defer();
                deferred.resolve(
                    {
                        timestamp: 234234234,
                        invoice: {
                            number: "234333333",
                            url: "http://www.precor.com/en-us"
                        },
                        customerSource: "Web",
                        managementCompany: "Xyz Management",
                        buyingGroup: "ABC Buying Group",
                        facility: {
                            name: "Test facility",
                            salesforceId: "2343kk23l2lj3k3",
                            address: {
                                line1: "222 snowflake lane",
                                line2: null,
                                city: "north pole",
                                region: "north region",
                                postalCode: "234233",
                                countryCode: "US"
                            },
                            phoneNumber: "2333333333",
                            customerSegmentLevel1: "Corporate",
                            customerSegmentLevel2: "Advantage Sport & Fitness",
                            customerSegmentLevel3: "Other",
                            brand: "Conrad Hotel & Resort",
                            subBrand: "Other",
                            contact: {
                                firstName: "Kris",
                                lastName: "Kringle",
                                phoneNumber: "8675309",
                                emailAddress: "kris@christmas.com"
                            }
                        },
                        repId: "rep@vendor.com",
                        lineItems: [
                            {
                                serialNumber: "AD23423333",
                                productGroup: "Strength - Commercial",
                                productLine: "Strength Plate Loaded Icarian",
                                price: 200.00
                            },
                            {
                                components: [
                                    {
                                        serialNumber: "A925D17130D01",
                                        productGroup: "Treadmill - Commercial",
                                        productLine: "Treadmill 921",
                                        price: 700.00
                                    },
                                    {
                                        serialNumber: "BD633A00F0DD",
                                        productGroup: "Treadmill Display - Commercial",
                                        productLine: "Treadmill P30 Display - Commercial",
                                        price: 500.00
                                    }
                                ]
                            }
                        ],
                        registrationId: registrationId,
                        registrationTimestamp: 234234234
                    });
                return deferred.promise;
            }
        }

        /**
         * Gets a partner commercial sale  by its registrationId
         * @param registrationId
         * @returns {PartnerCommercialSale}
         */
        function getPartnerCommercialSale(registrationId) {
            // use local data if we have it
            if (lastRegisteredPartnerCommercialSale
                && lastRegisteredPartnerCommercialSale.registrationId == registrationId) {
                var deferred = $q.defer();
                deferred.resolve(lastRegisteredPartnerCommercialSale);
                return deferred.promise;
            }
            else {
                // use dummy data for now
                var deferred = $q.defer();
                deferred.resolve(
                    {
                        timestamp: 234234234,
                        invoice: {
                            number: "234333333",
                            url: "http://www.precor.com/en-us"
                        },
                        customerSource: "Web",
                        managementCompany: "Xyz Management",
                        buyingGroup: "ABC Buying Group",
                        facility: {
                            name: "Test facility",
                            salesforceId: "2343kk23l2lj3k3",
                            address: {
                                line1: "222 snowflake lane",
                                line2: null,
                                city: "north pole",
                                region: "north region",
                                postalCode: "234233",
                                countryCode: "US"
                            },
                            phoneNumber: "2333333333",
                            customerSegmentLevel1: "Corporate",
                            customerSegmentLevel2: "Advantage Sport & Fitness",
                            customerSegmentLevel3: "Other",
                            brand: "Conrad Hotel & Resort",
                            subBrand: "Other",
                            contact: {
                                firstName: "Kris",
                                lastName: "Kringle",
                                phoneNumber: "8675309",
                                emailAddress: "kris@christmas.com"
                            }
                        },
                        repId: "rep@vendor.com",
                        lineItems: [
                            {
                                serialNumber: "AD23423333",
                                productGroup: "Strength - Commercial",
                                productLine: "Strength Plate Loaded Icarian",
                                price: 200.00
                            },
                            {
                                components: [
                                    {
                                        serialNumber: "A925D17130D01",
                                        productGroup: "Treadmill - Commercial",
                                        productLine: "Treadmill 921",
                                        price: 700.00
                                    },
                                    {
                                        serialNumber: "BD633A00F0DD",
                                        productGroup: "Treadmill Display - Commercial",
                                        productLine: "Treadmill P30 Display - Commercial",
                                        price: 500.00
                                    }
                                ]
                            }
                        ],
                        registrationId: registrationId,
                        registrationTimestamp: 234234234
                    });
                return deferred.promise;
            }

            /*
             var request = $http({
             method: "get",
             url: partnerSaleServiceConfig.baseUrl + "/partner-commercial-sale-s",
             params: {
             registrationId: registrationId
             }
             });

             return request
             .then(
             handleSuccess,
             handleError
             );
             */
        }

        /**
         * Gets a partner consumer sale  by its registrationId
         * @param registrationId
         * @returns {PartnerConsumerSale}
         */
        function getPartnerConsumerSale(registrationId) {
            // use local data if we have it
            if (lastRegisteredPartnerConsumerSale
                && lastRegisteredPartnerConsumerSale.registrationId == registrationId) {
                var deferred = $q.defer();
                deferred.resolve(lastRegisteredPartnerConsumerSale);
                return deferred.promise;
            }
            // otherwise get it from the server
            else {
                // use dummy data for now
                var deferred = $q.defer();
                deferred.resolve({
                    timestamp: 234234234,
                    invoice: {
                        number: "234333333",
                        url: "http://www.precor.com/en-us"
                    },
                    consumer: {
                        firstName: "testy",
                        lastName: "mctesterson",
                        address: {
                            line1: "1000 snowflake lane",
                            line2: null,
                            city: "north pole",
                            region: "north region",
                            postalCode: "239393",
                            countryCode: "US"
                        },
                        phoneNumber: "222-222-2222",
                        emailAddress: "test@test.com"
                    },
                    repId: "rep@vendor.com",
                    lineItems: [
                        {
                            serialNumber: "AD23423333",
                            productGroup: "Strength - Commercial",
                            productLine: "Strength Plate Loaded Icarian",
                            price: 200.00
                        },
                        {
                            components: [
                                {
                                    serialNumber: "A925D17130D01",
                                    productGroup: "Treadmill - Commercial",
                                    productLine: "Treadmill 921",
                                    price: 700.00
                                },
                                {
                                    serialNumber: "BD633A00F0DD",
                                    productGroup: "Treadmill Display - Commercial",
                                    productLine: "Treadmill P30 Display - Commercial",
                                    price: 500.00
                                }
                            ]
                        }
                    ],
                    registrationId: registrationId,
                    registrationTimestamp: 234234234
                });
                return deferred.promise;
            }

            /*
             var request = $http({
             method: "get",
             url: partnerSaleServiceConfig.baseUrl + "/partner-consumer-sale-s",
             params: {
             registrationId: registrationId
             }
             });

             return request
             .then(
             handleSuccess,
             handleError
             );
             */
        }

        /**
         * Lists the current partners commercial sale s since a given timestamp
         * @param timestamp
         * @returns {PartnerCommercialSaleSummary[]}
         */
        function listCurrentPartnerCommercialSalesRegisteredSinceTimestamp(timestamp) {

            // use dummy data for now
            var deferred = $q.defer();
            deferred.resolve(
                [
                    {
                        facilityName: "LA fitness",
                        facilityAddress: {
                            line1: "12321 120th Pl NE",
                            line2: null,
                            city: "Kirkland",
                            region: "WA",
                            postalCode: "98004",
                            countryCode: "US"
                        },
                        registrationTimestamp: 1432701017,
                        registrationId: "234213423"
                    },
                    {
                        facilityName: "Gold's Gym",
                        facilityAddress: {
                            line1: "12321 120th Pl NE",
                            line2: null,
                            city: "Kirkland",
                            region: "WA",
                            postalCode: "98188",
                            countryCode: "US"
                        },
                        registrationTimestamp: 1432706017,
                        registrationId: "224234423"
                    },
                    {
                        facilityName: "24 hr fitness",
                        facilityAddress: {
                            line1: "1000",
                            line2: null,
                            city: "Bellevue",
                            region: "WA",
                            postalCode: "98004",
                            countryCode: "US"
                        },
                        registrationTimestamp: 1432706017,
                        registrationId: "224233423"
                    }
                ]);
            return deferred.promise;

            /*
             var request = $http({
             method: "get",
             url: partnerSaleServiceConfig.baseUrl + "/partner-commercial-sale-s",
             params: {
             timestamp: timestamp
             }
             });

             return request
             .then(
             handleSuccess,
             handleError
             );
             */
        }

        /**
         * Registers a partner commercial sale
         * @param {PartnerCommercialSale} partnerCommercialSale
         * @returns {string} - a  registrationId
         */
        function registerPartnerCommercialSale(partnerCommercialSale) {
            // use dummy data for now
            var deferred = $q.defer();
            partnerCommercialSale.registrationId = new Date().getTime();
            partnerCommercialSale.registrationTimestamp = Math.floor(new Date().getTime() / 1000);
            lastRegisteredPartnerCommercialSale = partnerCommercialSale;
            deferred.resolve((new Date().getTime()));
            return deferred.promise;

            /*var request = $http({
             method: "post",
             url: partnerSaleServiceConfig.baseUrl + "/partners/current/commercial-sale-s",
             data: partnerCommercialSale
             });

             return request
             .then
             (
             handleSuccess,
             handleError
             )
             .then
             (
             function (registrationId) {
             // set  registrationId and timestamp and store for confirmation page
             partnerCommercialSale.registrationId = registrationId;

             partnerCommercialSale.registrationTimestamp =
             Math.floor(new Date().getTime()/1000);

             lastRegisteredPartnerCommercialSale = partnerCommercialSale;
             return registrationId;
             },
             function (reason) {
             return $q.reject(reason);
             }
             );*/
        }

        /**
         * Registers a partner consumer sale
         * @param {PartnerConsumerSale} partnerConsumerSale
         * @returns {string} - a  registrationId
         */
        function registerPartnerConsumerSale(partnerConsumerSale) {
            // use dummy data for now
            var deferred = $q.defer();
            partnerConsumerSale.registrationId = new Date().getTime();
            partnerConsumerSale.registrationTimestamp = Math.floor(new Date().getTime() / 1000);
            lastRegisteredPartnerConsumerSale = partnerConsumerSale;
            deferred.resolve((new Date().getTime()));
            return deferred.promise;

            /*var request = $http({
             method: "post",
             url: partnerSaleServiceConfig.baseUrl + "/partners/current/consumer-sale-s",
             data: partnerConsumerSale
             });

             return request
             .then
             (
             handleSuccess,
             handleError
             )
             .then
             (
             function (registrationId) {
             // set  registrationId and timestamp and store for confirmation page
             partnerConsumerSale.registrationId = registrationId;

             partnerConsumerSale.registrationTimestamp =
             Math.floor(new Date().getTime()/1000);

             lastRegisteredPartnerConsumerSale = partnerConsumerSale;
             return registrationId;
             },
             function (reason) {
             return $q.reject(reason);
             }
             );*/
        }

        /**
         * updates the invoice associated with a PartnerSale
         * @param {string} registrationId
         * @param {File} invoiceFile
         * @param {string} invoiceNumber
         */
        function updatePartnerSaleInvoice(registrationId,
                                          invoiceFile,
                                          invoiceNumber) {
            // use dummy data for now
            var deferred = $q.defer();
            deferred.resolve();
            return deferred.promise;
        }

        function handleError(response) {
            if (
                !angular.isObject(response.data) || !response.data.message
            ) {

                return ( $q.reject("An unknown error occurred.") );

            }

            // Otherwise, use expected error message.
            return ( $q.reject(response.data.message) );

        }

        function handleSuccess(response) {

            return ( response.data );

        }
    }
})
();
