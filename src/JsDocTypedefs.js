
/**
 * A mailing address
 * @typedef {Object} Address
 * @property {string} line1
 * @property {string} [line2]
 * @property {string} city
 * @property {string} region
 * @property {string} postalCode
 * @property {string} countryCode - iso 3166-1-alpha-2 country code
 */

/**
 * Information about a contact for a facility
 * @typedef {Object} FacilityContactInformation
 * @property {string} firstName
 * @property {string} lastName
 * @property {string} phoneNumber
 * @property {string} emailAddress
 */

/**
 * Information about a facility
 * @typedef {Object} FacilityInformation
 * @property {string} name
 * @property {string} salesforceId
 * @property {Address} address
 * @property {string} phoneNumber
 * @property {string} customerSegmentLevel1
 * @property {string} customerSegmentLevel2
 * @property {string} customerSegmentLevel3
 * @property {string} brand
 * @property {string} subBrand,
 * @property {FacilityContactInformation} contact
 */

/**
 * An invoice from a partners sale
 * @typedef {Object} PartnerSaleInvoice
 * @property {string} url
 * @property {string} number
 */

/**
 * A partner sale
 * @typedef {Object} PartnerSale
 * @property {number} timestamp
 * @property {PartnerSaleInvoice} invoice
 * @property {string} repId
 * @property {PartnerSaleLineItem[]} lineItems
 * @property {string} registrationId
 * @property {number} registrationTimestamp
 */

/**
 * A partner commercial sale
 * @typedef {Object} PartnerCommercialSale
 * @augments PartnerSale
 * @property {string} customerSource
 * @property {string} managementCompany
 * @property {string} buyingGroup
 * @property {FacilityInformation} facility
 */

/**
 * Information about a consumer
 * @typedef {Object} ConsumerInformation
 * @property {string} firstName
 * @property {string} lastName
 * @property {Address} address
 * @property {string} phoneNumber
 * @property {string} emailAddress
 */

/**
 * A partner consumer sale
 * @typedef {Object} PartnerConsumerSale
 * @augments PartnerSale
 * @property {ConsumerInformation} consumer
 */

/**
 * @typedef {Object} PartnerSaleLineItem
 * @property {number} [price]
 */

/**
 * @typedef {Object} PartnerSaleCompositeLineItem
 * @augments PartnerSaleLineItem
 * @property {PartnerSaleSimpleLineItem[]} [components]
 */

/**
 * @typedef {Object} PartnerSaleSimpleLineItem
 * @augments PartnerSaleLineItem
 * @property {string} productGroup
 * @property {string} productLine
 * @property {string} serialNumber
 * @property {string} description
 */

/**
 * A PartnerCommercialSale summary
 * @typedef {Object} PartnerCommercialSaleSummary
 * @property {string} facilityName
 * @property {string} facilityAddress
 * @property {string} registrationTimestamp
 * @property {string} registrationId
 */