## Description
An AngularJS Module implementing common use cases encountered when integrating AngularJS apps
 with the precorconnect partner sale service.

## UseCases

#####constructPartnerCommercialSale
Constructs a PartnerCommercialSale

#####constructPartnerConsumerSale
Constructs a PartnerConsumerSale

#####constructPartnerSaleCompositeLineItem
Constructs a PartnerSaleCompositeLineItem

#####constructPartnerSaleSimpleLineItem
Constructs a PartnerSaleSimpleLineItem

#####getPartnerSale
Gets a partner sale by its registration id

#####getPartnerCommercialSale
Gets a partner commercial sale by its registration id

#####getPartnerConsumerSale
Gets a partner consumer sale by its registration id

#####listCurrentPartnerCommercialSalesRegisteredSinceTimestamp
Lists the current partners commercial sales registered since a given timestamp

#####registerPartnerCommercialSale
Registers a partner commercial sale 

#####registerPartnerConsumerSale
Registers a partner consumer sale 

#####updatePartnerSaleInvoice
Updates the invoice associated with a PartnerSale

## Installation
add as bower dependency

```shell
bower install https://bitbucket.org/precorconnect/partner-sale-service-angularjs-sdk.git --save
```
include in view
```html
<script src="bower-components/angular/angular.js"></script>
<script src="bower-components/partner-sale-service-angularjs-sdk/dist/partner-sale-service-angularjs-sdk.js"></script>
```
configure
see below.

## Configuration
####Properties
| Name (* denotes required) | Description |
|------|-------------|
| baseUrl* | The base url of the partner sale  service. |

#### Example
```js
angular.module(
        "app",
        ["partnerSaleServiceModule"])
        .config(
        [
            "partnerSaleServiceConfigProvider",
            appConfig
        ]);

    function appConfig(partnerSaleServiceConfigProvider) {
        partnerSaleServiceConfigProvider
            .setBaseUrl("@@partnerSaleServiceBaseUrl");
    }
```